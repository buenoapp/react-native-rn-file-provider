# react-native-rn-file-provider

fileprovider implementation

## Installation

```sh
npm install react-native-rn-file-provider
```

## Usage

```js
import RnFileProvider from "react-native-rn-file-provider";

// ...

const result = await RnFileProvider.multiply(3, 7);
```

## Contributing

See the [contributing guide](CONTRIBUTING.md) to learn how to contribute to the repository and the development workflow.

## License

MIT
