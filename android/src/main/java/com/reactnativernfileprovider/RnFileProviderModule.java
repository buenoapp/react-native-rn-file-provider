package com.reactnativernfileprovider;

import androidx.annotation.NonNull;

import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.module.annotations.ReactModule;
import com.facebook.react.bridge.Callback;

import java.io.File;
import android.net.Uri;
import androidx.core.content.FileProvider;

@ReactModule(name = RnFileProviderModule.NAME)
public class RnFileProviderModule extends ReactContextBaseJavaModule {
    public static final String NAME = "RnFileProvider";

    public RnFileProviderModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    @NonNull
    public String getName() {
        return NAME;
    }


    // Example method
    // See https://reactnative.dev/docs/native-modules-android
    @ReactMethod
    public void getUriForFile(String authority, String filepath, Promise promise) {
        try {
          // replace file-protocol if present
          filepath = filepath.replace("file://", "");
          //
          File file = new File(filepath);
          if (!file.exists()) throw new Exception("File does not exist");
          Uri contentUri = FileProvider.getUriForFile(this.getReactApplicationContext(), authority, file);
          promise.resolve(contentUri.toString());
        } catch (Exception ex) {
          ex.printStackTrace();
          reject(promise, filepath, ex);
        }
      }

      private void reject(Promise promise, String filepath, Exception ex) {
        promise.reject(null, ex.getMessage());
      }
}
