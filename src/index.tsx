import { NativeModules } from 'react-native';

const { RnFileProvider } = NativeModules;

export { RnFileProvider };
